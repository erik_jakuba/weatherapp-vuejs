import WeatherDetails from './components/WeatherDetails.vue'
import WeekWeather from './components/WeekWeather.vue'

export const routes = [
  {path: '/currentForecast', component: WeatherDetails},
  {path: '/weekForecast', component: WeekWeather}

]
