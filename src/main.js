import Vue from 'vue'
import App from './App.vue'
import vuetify from './plugins/vuetify';
import VueRouter from 'vue-router'
import store from './store.js'
import {routes} from './router.js'

Vue.use(VueRouter);

Vue.config.productionTip = false

const router = new VueRouter({
  routes,
  mode: 'history',
})


new Vue({
  store,
  router,
  vuetify,
  render: h => h(App)
}).$mount('#app')
