import Vue from 'vue'
import Vuex from 'vuex'
import moment from "moment";

Vue.use(Vuex);

export default new Vuex.Store({
    state: {
        temperature: '',
        tempKelvin: '',
        city: '',
        icon: '',
        description: '',
        isEnterPressed: false,
        humidity: '',
        wind: '',
        cloudindess: '',
        sunriseLink: 'https://cdn4.iconfinder.com/data/icons/the-weather-is-nice-today/64/weather_27-48.png',
        sunsetLink: 'https://cdn4.iconfinder.com/data/icons/the-weather-is-nice-today/64/weather_26-48.png',
        sunset: '',
        sunrise: '',
        weekWeather: [],
        monday: [],
        tuesday: [],
        wednesday: [],
        thursday: [],
        friday: [],
        saturday: [],
        sunday: [],
        currentDay: '',
        footerBarState: false,

    },
    mutations: {
        temperatureMutation(state, payload) {
            state.temperature = Math.round(payload - 272.15);
        }
        ,
        temeperatureKelvinMutation(state, payload) {
            state.tempKelvin = payload;
        }
        ,
        cityMutation(state, payload) {
            state.city = payload;
        }
        ,
        iconMutation(state, payload) {
            state.icon = 'http://openweathermap.org/img/wn/' + payload + '@2x.png';
        }
        ,
        descMutation(state, payload) {
            state.description = payload;
        }
        ,
        humidityMutation(state, payload) {
            state.humidity = payload;
        }
        ,
        windMutation(state, payload) {
            state.wind = Math.round(payload * 3.6);
        }
        ,
        cloudinessMutation(state, payload) {
            state.cloudindess = payload;
        }
        ,
        sunriseMutation(state, payload) {
            state.sunrise = payload;
        }
        ,
        sunsetMutation(state, payload) {
            state.sunset = payload;
        }
        ,
        setWeekWeatherMutation(state, payload) {
            const days = ['Sunday', 'Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday'];
            state.monday = [];
            state.tuesday = [];
            state.wednesday = [];
            state.thursday = [];
            state.friday = [];
            state.saturday = [];
            state.sunday = [];

            for (let key in payload) {
                const day = days[moment.unix(payload[key].dt).day()];
                if (day === 'Monday') {
                    state.monday.push(payload[key]);
                }
                if (day === 'Tuesday') {
                    state.tuesday.push(payload[key]);
                }
                if (day === 'Wednesday') {
                    state.wednesday.push(payload[key]);
                }
                if (day === 'Thursday') {
                    state.thursday.push(payload[key]);
                }
                if (day === 'Friday') {
                    state.friday.push(payload[key]);
                }
                if (day === 'Saturday') {
                    state.saturday.push(payload[key]);
                }
                if (day === 'Sunday') {
                    state.sunday.push(payload[key]);
                }
            }

        },
        setCurrentDayMutation(state) {
            const days = ['Sunday', 'Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday'];
            const day = days[moment().day()];
            state.currentDay = day;
        }
    }
    ,
    actions: {
        setWeather({commit}, payload) {
            commit('temperatureMutation', payload.main.temp);
            commit('iconMutation', payload.weather[0].icon);
            commit('descMutation', payload.weather[0].description);
            commit('humidityMutation', payload.main.humidity);
            commit('windMutation', payload.wind.speed);
            commit('cloudinessMutation', payload.clouds.all);
            commit('sunriseMutation', moment.unix(payload.sys.sunrise).format("HH:mm"));
            commit('sunsetMutation', moment.unix(payload.sys.sunset).format("HH:mm"));
        }
        ,
        setWeekWeather({commit}, payload) {
            commit('setWeekWeatherMutation', payload);
            commit('setCurrentDayMutation');
        }
    }
    ,
    getters: {
        getTemperature: state => {
            return state.temperature;
        },
        getCity:
            state => {
                return state.city;
            },
        getIconLink:
            state => {
                return state.icon;
            },
        getDescription:
            state => {
                return state.description;
            },
        getKelvin:
            state => {
                return state.tempKelvin;
            },
        getHumidity:
            state => {
                return state.humidity;
            },
        getWind:
            state => {
                return state.wind;
            },
        getCloudiness:
            state => {
                return state.cloudindess;
            },
        getSunrise:
            state => {
                return state.sunrise;
            },
        getSunset:
            state => {
                return state.sunset;
            },
        getActualDay: state => {
            if (state.currentDay === 'Sunday') {
                return state.sunday;
            }
            if (state.currentDay === 'Monday') {
                return state.monday;
            }
            if (state.currentDay === 'Tuesday') {
                return state.tuesday;
            }
            if (state.currentDay === 'Wednesday') {
                return state.wednesday;
            }
            if (state.currentDay === 'Thursday') {
                return state.thursday;
            }
            if (state.currentDay === 'Friday') {
                return state.friday;
            }
            if (state.currentDay === 'Saturday') {
                return state.saturday;
            }
        },
        getfooterBarState:state =>{
            return state.footerBarState;
        }
    }
})
;
